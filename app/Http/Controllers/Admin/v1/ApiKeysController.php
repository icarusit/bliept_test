<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Controllers\Controller;
use App\Models\Utils\ApiKey;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ApiKeysController extends Controller {

    public function getList()
    {
        $data = [
            'keys' => ApiKey::all()
        ];

        return view('admin.v1.api-keys.list')->with($data);
    }

    public function getNew()
    {
        $key = new ApiKey();
        $key->api_key = \Uuid::generate(4);

        $data = [
            'key' => $key
        ];

        return view('admin.v1.api-keys.edit')->with($data);
    }

    public function getEdit($id)
    {
        $data = [
            'key' => ApiKey::find($id)
        ];

        return view('admin.v1.api-keys.edit')->with($data);
    }

    public function postSave(Request $request)
    {
        $apikey = ApiKey::updateOrCreate(['id' => $request->input('id')], [
            'api_key' => $request->input('api_key'),
            'ip_adresses' => $request->input('ip_adresses'),
            'description' => $request->input('description'),
            'full_rights' => $request->input('full_rights')?1:0
        ]);

        $request->input('auth') ? $apikey->addRight('auth') : $apikey->removeRight('auth');
        $request->input('account') ? $apikey->addRight('account') : $apikey->removeRight('account');
        $request->input('webshop') ? $apikey->addRight('webshop') : $apikey->removeRight('webshop');
        $request->input('scorecard') ? $apikey->addRight('scorecard') : $apikey->removeRight('scorecard');
        $request->input('mail') ? $apikey->addRight('mail') : $apikey->removeRight('mail');

        return redirect('admin/v1/api-keys/list');
    }

    public function getDelete($id)
    {
        ApiKey::find($id)->delete();
        return redirect('admin/v1/api-keys/list');
    }

    public function postGetKey(Request $request)
    {
        return ApiKey::find($request->input('keyId'))->api_key;
    }
}