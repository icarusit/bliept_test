<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Controllers\Controller;
use App\Models\Utils\Advertention\Campaign;
use App\Models\Utils\Advertention\Image;
use Illuminate\Http\Request;

class AdvertentionController extends Controller {

	public function __construct()
	{
		ini_set('memory_limit', -1);
	}

	public function getIndex()
	{
		$data = [
			'campaigns' => Campaign::all()
		];

		return view('admin.v1.ads.list-campaigns')->with($data);
	}

	/**
	 * Campaigns
	 */

	public function getCreateCampaign()
	{
		$data = [
			'campaign' => new Campaign()
		];

		return view('admin.v1.ads.edit-campaign')->with($data);
	}

	public function getEditCampaign($id)
	{
		$data = [
			'campaign' => Campaign::find($id)
		];

		return view('admin.v1.ads.edit-campaign')->with($data);
	}

	public function getDeleteCampaign($id)
	{
		Campaign::where('id', '=', $id)->delete();
		return redirect('admin/v1/ads');
	}

	public function postSaveCampaign(Request $request)
	{
		Campaign::updateOrCreate(['id' => $request->input('id')], [
			'title' => $request->input('title'),
			'description' => $request->input('description'),
			'customer' => $request->input('customer'),
			'website_url' => $request->input('website_url'),
			'active' => $request->input('active')?1:0
		]);

		return redirect('admin/v1/ads');
	}

	public function getViewCampaign($id)
	{
		$campaign = Campaign::find($id);
		$data = [
			'campaign' => $campaign,
			'campaign_id' => $campaign->id
		];

		return view('admin.v1.ads.view-campaign')->with($data);
	}

	/**
	 * Images
	 */

	public function getNewImage($campaign_id)
	{
		$image = new Image();
		$image->campaign_id = $campaign_id;

		$data = [
			'image' => $image
		];

		return view('admin.v1.ads.image.edit-image')->with($data);
	}

	public function getEditImage($id)
	{
		$data = [
			'image' => Image::find($id)
		];

		return view('admin.v1.ads.image.edit-image')->with($data);
	}

	public function getDeleteImage($id)
	{
		$image = Image::find($id, ['id', 'campaign_id']);
		$image->delete();

		return redirect('admin/v1/ads/view-campaign/'.$image->campaign_id);
	}

	public function postSaveImage(Request $request)
	{
		$image = Image::updateOrCreate(['id' => $request->input('id')], [
			'campaign_id' => $request->input('campaign_id'),
			'image_url' => $request->input('image_url'),
			'website_url' => $request->input('website_url')
		]);

		if(!$image->encoded_image_url)
			$image->encoded_image_url = url('api/v1/public/ads/register-impression/'.$image->id.'/'.$image->campaign_id.'/'.str_replace('/', '', \Hash::make(time())).'.jpg');
		if(!$image->encoded_website_url)
			$image->encoded_website_url = url('api/v1/public/ads/register-ctr/'.$image->id.'/'.$image->campaign_id.'/'.str_replace('/', '', \Hash::make(time()*2)));

		$image->save();
		return redirect('admin/v1/ads/view-campaign/'.$request->input('campaign_id'));
	}
}