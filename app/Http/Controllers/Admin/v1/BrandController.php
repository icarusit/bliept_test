<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class BrandController extends Controller {

    public function getList()
    {
        $data = [
            'brands' => Brand::all()
        ];
        
        return view('admin.v1.brand.list')->with($data);
    }

    public function getNew()
    {
        $data = [
            'brand' => new Brand()
        ];

        return view('admin.v1.brand.edit')->with($data);
    }

    public function getEdit($id)
    {
        $data = [
            'brand' => Brand::find($id)
        ];

        return view('admin.v1.brand.edit')->with($data);
    }

    public function getDelete($id)
    {
        Brand::find($id)->delete();
        return redirect()->back();
    }

    public function postSave(Request $request)
    {
        $id = $request->input('id') ?: Uuid::generate(4);

        $brand = Brand::find($id);
        if(!$brand)
        {
            $brand = new Brand();
            $brand->id = $id;
        }

        $brand->name = $request->input('name');
        $brand->description = $request->input('description');
        $brand->thumbnail_url = $request->input('thumbnail_url');
        $brand->facebook_url = $request->input('facebook_url');
        $brand->twitter_url = $request->input('twitter_url');
        $brand->instagram_url = $request->input('instagram_url');
        $brand->youtube_url = $request->input('youtube_url');
        $brand->web_url = $request->input('web_url');
        $brand->webshop_category_url = $request->input('webshop_category_url');

        $brand->save();

        return redirect()->route('admin.brand.list');
    }

}