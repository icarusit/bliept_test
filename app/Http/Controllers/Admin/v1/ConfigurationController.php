<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Controllers\Controller;
use App\Models\Utils\ConfigValue;
use Illuminate\Http\Request;

class ConfigurationController extends Controller {

    public function getIndex()
    {
        $data = [
            'configs' => ConfigValue::all()
        ];

        return view('admin.v1.configuration.list')->with($data);
    }

    public function getAdd()
    {
        $data = [
            'config' => new ConfigValue()
        ];

        return view('admin.v1.configuration.edit')->with($data);
    }

    public function getEdit($id)
    {
        $data = [
            'config' => ConfigValue::find($id)
        ];

        return view('admin.v1.configuration.edit')->with($data);
    }

    public function getDelete($id)
    {
        ConfigValue::where('id', '=', $id)->delete();
        return redirect('admin/v1/configuration');
    }

    public function postSave(Request $request)
    {
        ConfigValue::updateOrCreate(['id' => $request->input('id')], [
            'friendly_name' => $request->input('friendly_name'),
            'system_name' => $request->input('system_name'),
            'value' => $request->input('value')
        ]);

        return redirect('admin/v1/configuration');
    }

    public static function getFlushCache()
    {
        \Cache::flush();
        return redirect('admin/v1/configuration');
    }
}