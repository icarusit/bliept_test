<?php

namespace App\Http\Controllers\Admin\v1;

use App\Http\Controllers\Controller;

class CompetitionsController extends Controller {

    public function getList()
    {
        $competitions = \DB::connection('cds')
            ->select('SELECT count(cmp_form_internal_code) as qty, cmp_form_internal_code, cmp_form_name
            FROM c_cmp_competitions GROUP BY cmp_form_internal_code');

        $data = [
            'competitions' => $competitions
        ];

        return view('admin.v1.competitions.list')->with($data);
    }

    public function getExport($cmp_form_internal_code)
    {
        $participants = \DB::connection('cds')
            ->select("SELECT * from c_cmp_competitions WHERE cmp_form_internal_code='".$cmp_form_internal_code."'");

        return \Excel::create('participants', function($excel) use ($participants){
            $excel->sheet('Participants', function($sheet) use($participants) {
                $participants = json_encode($participants);
                $participants = json_decode($participants, true);

                $endArray = [];

                foreach($participants as $participant)
                {
                    $answers = json_decode($participant['answers'], true);

                    array_forget($participant, 'answers');
                    foreach($answers as $key => $answer)
                    {
                        if($key == 12)
                        {
                            $participant['country'] = $answer['value']['countryIso'];
                            $participant['street'] = $answer['value']['streetName'];
                            $participant['number'] = $answer['value']['streetNumber'];
                            $participant['postal_code'] = $answer['value']['postalCode'];
                            $participant['city'] = $answer['value']['city'];
                        }
                        else
                        {
                            $participant[$answer['name']] = $answer['value'];
                        }
                    }

                    array_push($endArray, $participant);

                    //dd($participant);
                }

                $sheet->fromArray($endArray);
            });
        })->download('xlsx');
    }
}