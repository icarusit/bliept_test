<?php

namespace App\Http\Controllers\Admin\v1\Webshop;

use App\Http\Controllers\Controller;
use App\Models\Lookups\WebshopBrandId;
use Illuminate\Http\Request;

class BrandIdsController extends Controller {

    public function getIndex()
    {
        $data = [
            'brandIds' => WebshopBrandId::all()
        ];

        return view('admin.v1.webshop.brand-ids')->with($data);
    }

    public function getDelete($id)
    {
        WebshopBrandId::find($id)->delete();

        return redirect('admin/v1/webshop/brand-ids');
    }

    public function postSave(Request $request)
    {
        WebshopBrandId::create([
            'name' => $request->input('name'),
            'webshop_brand_id' => $request->input('webshop_brand_id'),
            'sf_brand_id' => $request->input('sf_brand_id')
        ]);

        return redirect('admin/v1/webshop/brand-ids');
    }

}