<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class AuthController extends Controller {

    protected $auth;

    public function __construct(Guard $guard)
    {
        $this->auth = $guard;
    }

    public function getIndex()
    {
        if(!$this->auth->check())
        {
            return redirect('auth/login');
        }
        else
        {
            return redirect('admin/dashboard');
        }
    }

    public function getLogin()
    {
        if($this->auth->check())
            return redirect('admin/dashboard');

        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $rememberMe = $request->input('remember_me');

        if($this->auth->attempt(['email' => $email, 'password' => $password], $rememberMe))
        {
            return redirect('admin/dashboard');
        }
        else
        {
            return redirect()->guest('auth/invalid-login');
        }
    }

    public function getInvalidLogin()
    {
        return view('admin.invalid-login');
    }

    public function getLogout()
    {
        $this->auth->logout();
        return redirect()->guest('auth/login');
    }
}