<?php

namespace App\Http\Controllers\Api\v1\PubApi;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Utils\Advertention\Ctr;
use App\Models\Utils\Advertention\Image;
use App\Models\Utils\Advertention\Impression;
use Illuminate\Http\Request;

class AdvertentionController extends BaseApiController {

    public function getRegisterImpression($image_id, $campaign_id, $encoded, Request $request)
    {
        $image = Image::find($image_id, ['id', 'impressions', 'image_url']);
        $image->impressions++;
        $image->save();

        $imgInfo = getimagesize($image->image_url);
        header("Content-Type: ".$imgInfo['mime']);

        readfile($image->image_url);
    }

    public function getRegisterCtr($image_id, $campaign_id, $encoded, Request $request)
    {
        $image = Image::find($image_id, ['id', 'clicks', 'website_url']);
        $image->clicks++;
        $image->save();

        return redirect($image->website_url);
    }

}
