<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseApiController;
use App\Models\Epg\JuniorTVBroadcast;
use App\Models\Epg\JuniorTVProgram;
use App\Models\Epg\NjamTV;
use App\Models\Epg\Studio100TV;
use DB;
use Illuminate\Http\Request;

class EpgController extends BaseApiController {

    public function __construct(Request $request)
    {
        date_default_timezone_set('Europe/Brussels');
        //setlocale(LC_ALL, 'nl_NL.utf8');

        parent::__construct($request);
    }

    public function postTwoProgramsSchedule()
    {
        switch($this->request->input('channel'))
        {
            case "juniortv":
            {
                $firstProgram = JuniorTVBroadcast::whereRaw('? BETWEEN `displayed_time_start` and `displayed_time_end`', array(date('Y-m-d H:i:s')))->first();
                if(!$firstProgram)
                    $firstProgram = JuniorTVBroadcast::first();

                $firstProgram->toArray();

                if(!$firstProgram)
                    return json_encode(['error' => 'The current program could not be found.', 'now' => [], 'next' => []]);

                $nextProgram = JuniorTVBroadcast::where('displayed_time_start', '>=', date('Y-m-d H:i:s', strtotime($firstProgram['displayed_time_end'])))->orderBy('displayed_time_start', 'asc')->first()->toArray();

                if(!$nextProgram)
                    return json_encode(['error' => 'The next program could not be found.', 'now' => [], 'next' => []]);

                $fTheProgram = JuniorTVProgram::where('program_reference_number', '=', $firstProgram['program_reference_number'])->first(['resource', 'title']);
                $nTheProgram = JuniorTVProgram::where('program_reference_number', '=', $nextProgram['program_reference_number'])->first(['resource', 'title']);

                $firstProgram['title'] = $fTheProgram->title;
                $firstProgram['image'] = $fTheProgram->resource;
                $nextProgram['title'] = $nTheProgram->title;
                $nextProgram['image'] = $nTheProgram->resource;

                return json_encode(['now' => $firstProgram, 'next' => $nextProgram]);

            }break;

            case "njamtv":
            {
                $firstProgram = NjamTV::whereRaw('? BETWEEN `start_time` and `end_time`', array(date('Y-m-d H:i:s')))->first();

                if(!$firstProgram)
                    return json_encode([ 'error' => 'The current program could not be found.', 'now' => [], 'next' => []]);

                $nextProgram = NjamTV::where('start_time', '>=', date('Y-m-d H:i:s', strtotime('-7 minutes', strtotime($firstProgram->end_time))))->orderBy('start_time', 'asc')->first();

                if(!$nextProgram)
                    return json_encode([ 'error' => 'The next program could not be found.', 'now' => [], 'next' => []]);

                return json_encode(['now' => $firstProgram, 'next' => $nextProgram]);
            }break;

            case "st100tv":
            default:
            {
                $firstProgram = Studio100TV::whereRaw('? BETWEEN `start_time` and `end_time`', array(date('Y-m-d H:i:s')))->first();

                if(!$firstProgram)
                    return json_encode([ 'error' => 'The current program could not be found.', 'now' => [], 'next' => []]);

                $nextProgram = Studio100TV::where('start_time', '>=', date('Y-m-d H:i:s', strtotime('-7 minutes', strtotime($firstProgram->end_time))))->orderBy('start_time', 'asc')->first();

                if(!$nextProgram)
                    return json_encode([ 'error' => 'The next program could not be found.', 'now' => [], 'next' => []]);

                return json_encode(['now' => $firstProgram, 'next' => $nextProgram]);
            }
        }
    }

    public function postScheduleForDay()
    {
        switch($this->request->input('channel'))
        {
            case "juniortv":
            {
                if(!$day = $this->request->input('day'))
                    return json_encode('ERROR: NOT ENOUGH ARGUMENTS');

                $epg = JuniorTVBroadcast::where('displayed_time_start', 'LIKE', date('Y-m-d', strtotime($day)).'%')->remember(120)->get();
                return json_encode($epg);
            }break;

            case "njamtv":
            {
                if(!$day = $this->request->input('day'))
                    return json_encode('ERROR: NOT ENOUGH ARGUMENTS');

                $epg = NjamTV::whereOnscreenDate(date('Y-m-d', strtotime($day)))->where('start_time', '>=', date('Y-m-d', strtotime($day)) . ' 06:00:00')->remember(120)->get();
                return json_encode($epg);
            }break;

            case "st100tv":
            default:
            {
                if(!$day = $this->request->input('day'))
                    return json_encode('ERROR: NOT ENOUGH ARGUMENTS');

                $epg = Studio100TV::whereOnscreenDate(date('Y-m-d', strtotime($day)))->where('start_time', '>=', date('Y-m-d', strtotime($day)) . ' 06:00:00')->remember(120)->get();
                return json_encode($epg);
            }break;
        }

        return json_encode('ERROR');
    }

    public function postAvailableDays()
    {
        date_default_timezone_set('Europe/Brussels');

        if($this->request->get('channel') == 'st100tv')
        {
            setlocale(LC_ALL, 'nl_NL.utf8');
            $days = Studio100TV::remember(120)->distinct()->get(['onscreen_date']);

            $retArray = array();

            foreach($days as $day)
            {
                array_push($retArray, [
                    'date' => date('d-m-Y', strtotime($day->onscreen_date)),
                    'name' => strftime('%A', strtotime($day->onscreen_date))
                ]);
            }
        }
        elseif($this->request->get('channel') == 'njamtv')
        {
            setlocale(LC_ALL, 'nl_NL.utf8');
            $days = NjamTV::remember(120)->distinct()->get(['onscreen_date']);

            $retArray = array();

            foreach($days as $day)
            {
                array_push($retArray, [
                    'date' => date('d-m-Y', strtotime($day->onscreen_date)),
                    'name' => strftime('%A', strtotime($day->onscreen_date))
                ]);
            }
        }
        else
        {
            setlocale(LC_ALL, 'de_DE.utf8');
            $days = JuniorTVBroadcast::distinct('displayed_time_start')->get(['displayed_time_start']);
            $retArray = array();
            foreach($days as $day)
            {
                if(!in_array(date('d-m-Y', strtotime($day->displayed_time_start)), $retArray))
                {
                    array_push($retArray, [
                        'date' => date('d-m-Y', strtotime($day->displayed_time_start)),
                        'name' => strftime('%A', strtotime($day->displayed_time_start))
                    ]);
                }
            }
        }
        $retArray = array_map("unserialize", array_unique(array_map("serialize", $retArray)));
        return json_encode($retArray);
    }

    public function postJuniorPrograms()
    {
        return JuniorTVProgram::all()->toJson();
    }

    public function postStudio100tvPrograms()
    {
        $model = new Studio100TV();
        $programs = DB::table($model->getTable())->groupBy('program_id')->select([
            'program_id',
            'day_name',
            'show_title',
            'show_year',
            'country',
            'episode_title',
            'episode_number',
            'episode_count',
            'jaargang',
            'cast',
            'synopsis',
            'category',
            'tn_replay_rights',
            'tn_replay_days',
        ]);

        return json_encode($programs->get());
    }

    public function postNjamtvPrograms()
    {
        $model = new NjamTV();
        $programs = DB::table($model->getTable())->groupBy('program_id')->select([
            'program_id',
            'day_name',
            'show_title',
            'show_year',
            'country',
            'episode_title',
            'episode_number',
            'episode_count',
            'jaargang',
            'cast',
            'synopsis',
            'category',
            'tn_replay_rights',
            'tn_replay_days',
        ]);

        return json_encode($programs->get());
    }

}