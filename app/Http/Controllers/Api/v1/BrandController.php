<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseApiController;
use App\Libraries\Api\ResponseCodes;
use App\Libraries\SalesForce\Objects\Brand;

class BrandController extends BaseApiController {

    public function getList()
    {
        $brands = Brand::getActiveBrands();

        if(count($brands) <= 0)
        {
            return ResponseCodes::NothingFound();
        }

        $returnArray = [];

        foreach($brands as $brand)
        {
            array_push($returnArray, [
                'id' => $brand->External_Id__c,
                'name' => $brand->Name
            ]);
        }

        return json_encode($returnArray);
    }

}