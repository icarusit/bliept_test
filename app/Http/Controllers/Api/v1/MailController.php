<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseApiController;
use App\Libraries\Api\ResponseCodes;
use Illuminate\Mail\Message;

class MailController extends BaseApiController {

    public function postSendMail()
    {
        $validator = \Validator::make($this->request->all(), [
            'to' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        if($validator->fails())
            return ResponseCodes::InvalidData();

        $subject = $this->request->input('subject');
        $to = $this->request->input('to');
        $from = $this->request->input('from');

        \Mail::send('plain', ['body' => $this->request->input('message')], function(Message $message) use ($subject, $to, $from){
            if($from)
                $message->from($from);

            $message->to($to);
            $message->subject($subject);
        });

        return \Response::make('OK', 200);
    }

}