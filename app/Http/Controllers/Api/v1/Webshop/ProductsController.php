<?php

namespace App\Http\Controllers\Api\v1\Webshop;

use App\Http\Controllers\Api\BaseApiController;
use App\Libraries\Api\ResponseCodes;
use App\Libraries\Helper;
use App\Models\Lookups\WebshopBrandId;
use GuzzleHttp\Client;

class ProductsController extends BaseApiController {

    /*
     * Incoming example:
     * {
          "id_product":"844",
          "grade":"4.230769230769231",
          "sales":"1881",
          "name_nl":"K3 : CD - 10.000 luchtballonnen",
          "link_nl":"k3-cd-10000-luchtballonnen",
          "name_fr":"K3 : CD - 10.000 luchtballonnen",
          "link_fr":"k3-cd-10000-luchtballonnen",
          "id_image":"2469",
          "price":"16.99",
          "quantity":"391",
          "label_featured":"Reserveer nu!",
          "hard_to_find":"0",
          "sp_price":null,
          "sp_reduction":null,
          "sp_reduction_type":null
       },
     */

    public function getBestSellers()
    {
        $validator = \Validator::make($this->request->all(), [
            'image_size' => 'required',
            'language' => 'required',
            'country' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        return \Cache::remember('webshop:products:bestsellers:'.$this->request->input('brand_id').':'.$this->request->input('country').':'.$this->request->input('language'). ':' .$this->request->input('image_size'), 120, function(){
            $client = new Client();

            $additions = '';
            if($this->request->input('brand_id'))
            {
                $webshop_brand_id = WebshopBrandId::getWebshopBrandID($this->request->input('brand_id'));

                if($webshop_brand_id === false)
                    return ResponseCodes::BrandNotFound();

                $additions = '&brand='.$webshop_brand_id;
            }

            $res = $client->request('GET', 'https://studio100.com/webshop/syncs/s100/best-sellers.php?token=DPvqvGt6MD9GYk4AzqGTDqvKQhFcohWAFZTNBxRUAcAMCT8WUV'.$additions, []);

            if($res->getStatusCode() != 200 || empty($res->getBody()) || !Helper::is_json($res->getBody()))
            {
                return ResponseCodes::ThirdPartyProblem();
            }

            $body = $res->getBody();
            $resArray = json_decode($body);
            $newArray = array();

            foreach($resArray as $productObject)
            {
                $productObjectArray = [];
                //$productObjectArray = (array)$productObject;

                $linkName = 'link_' . $this->request->input('language');
                $name = 'name_'.$this->request->input('language');

                $productObjectArray['id_product'] = $productObject->id_product;
                $productObjectArray['sales'] = $productObject->sales;
                $productObjectArray['name'] = $productObject->$name;
                $productObjectArray['price'] = $productObject->price;
                $productObjectArray['grade'] = $productObject->grade;

                if($productObject->sp_reduction != null && $productObject->sp_reduction_type == 'amount')
                {
                    $productObjectArray['price_promo'] = (string)($productObject->price - $productObject->sp_reduction);
                }
                else
                {
                    $productObjectArray['price_promo'] = null;
                }

                $productObjectArray['quantity'] = $productObject->quantity;
                $productObjectArray['hard_to_find'] = $productObject->hard_to_find;
                $productObjectArray['is_new'] = $productObject->is_new;
                $productObjectArray['is_hard_to_find'] = $productObject->is_hard_to_find;
                $productObjectArray['is_coming_soon'] = $productObject->is_coming_soon;
                $productObjectArray['is_clearance'] = $productObject->is_clearance;


                $productObjectArray['product_url'] = "http://studio100.com/webshop/".$this->request->input('country')."/".$this->request->input('language')."/".$productObject->id_product."-".$productObject->$linkName.".html";
                $productObjectArray['image_url'] = "http://studio100.com/webshop/".$this->request->input('country')."/".$productObject->id_image."-".$this->request->input('image_size')."/".$productObject->$linkName.".jpg";


                array_push($newArray, $productObjectArray);
            }
            return json_encode($newArray);
        });
    }

}