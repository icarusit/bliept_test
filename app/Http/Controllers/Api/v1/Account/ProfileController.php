<?php

namespace App\Http\Controllers\Api\v1\Account;

use App\Http\Controllers\Api\BaseApiController;
use App\Libraries\Api\ResponseCodes;
use App\Libraries\Helper;
use App\Models\Account\Account;
use Exception;
use Illuminate\Http\Response;

class ProfileController extends BaseApiController {

    public function getIndex($userId)
    {
        if(is_null($userId))
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::find($userId, [
            'firstname', 'lastname', 'middlename', 'gender', 'birthdate', 'email', 'home_phone',
            'mobile_phone', 'description', 'address_street', 'address_number', 'address_postal_code', 'address_city',
            'company', 'company_email', 'vat_number', 'company_vat_number', 'facebook_id', 'google_plus_id',
            'instagram_id', 'twitter_id', 'linkedin_id', 'job_title', 'subscribe_status',

            'account_type', 'parent_email', 'parent_account_id', 'language_id', 'country_id'
        ]);
        if(!$account)
        {
            return ResponseCodes::UserIdInvalid();
        }

        return Response::create($account->toJson(), 200);
    }

    public function putIndex()
    {
        $validator = \Validator::make($this->request->all(), [
            'email' => 'required|email',
        ]);

        if($validator->fails())
        {
            return ResponseCodes::UserEmailInvalid();
        }

        if(Account::emailExists($this->request->input('email')))
            return ResponseCodes::UserAlreadyExists();

        $newId = \Uuid::generate(4);

        $account = new Account();
        $account->id = $newId;

        foreach($this->request->all() as $key => $inputVal)
        {
            if($key == 'apiKey')
                continue;

            $account->$key = $inputVal;
        }

        try {
            $account->save();
        } catch ( Exception $e ) {
            return ResponseCodes::InvalidData();
        }
        

        return Response::create($newId, 200);
    }

    public function postIndex($userId)
    {
        if(is_null($userId))
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::find($userId);

        if(!$account)
        {
            return ResponseCodes::UserIdInvalid();
        }

        foreach($this->request->all() as $key => $inputVal)
        {
            if($key == 'apiKey')
                continue;

            if($key == 'password')
                $inputVal = \Hash::make($inputVal);

            $account->$key = $inputVal;
        }

        $account->save();

        return Response::create(json_encode(true), 200);
    }

    public function postActivate()
    {
        $validator = \Validator::make($this->request->all(), [
            'userId' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::find($this->request->input('userId'));
        if(!$account)
        {
            return ResponseCodes::UserIdInvalid();
        }

        if($account->is_active)
        {
            return ResponseCodes::UserAlreadyActivated();
        }

        $account->is_active = true;
        $account->save();

        return Response::create(json_encode(true), 200);
    }
}