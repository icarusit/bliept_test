<?php

namespace App\Http\Controllers\Api\v1\Account;

use App\Events\Account\PasswordChangeRequest;
use App\Http\Controllers\Api\BaseApiController;
use App\Libraries\Api\ResponseCodes;
use App\Models\Account\Account;
use App\Models\Account\PasswordChange;
use App\Models\Account\PasswordReset;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class AuthController extends BaseApiController {

    public function postAuthenticate()
    {
        \Log::alert('/account/auth/authenticate');

        $validator = \Validator::make($this->request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::byEmail($this->request->input('email'));
        if(!$account)
        {
            \Log::alert('/account/auth/authenticate - Login Failed');
            return ResponseCodes::InvalidLogin();
        }

        if(!\Hash::check($this->request->input('password'), $account->password))
        {
            \Log::alert('/account/auth/authenticate - Login Failed');
            return ResponseCodes::InvalidLogin();
        }

        if(is_null($account->is_active) || !$account->is_active)
        {
            \Log::alert('/account/auth/authenticate - Account Inactive');
            return ResponseCodes::AccountInactive();
        }

        \Log::alert('/account/auth/authenticate - Login Succeeded - '.$account->id);
        return Response::create($account->id, 200);
    }

    public function postEmailExists()
    {
        $validator = \Validator::make($this->request->all(), [
            'email' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        return Response::create(json_encode(Account::emailExists($this->request->input('email'))), 200);
    }

    public function postChangePassword()
    {
        $validator = \Validator::make($this->request->all(), [
            'userId' => 'required',
            'oldPassword' => 'required',
            'newPassword' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::find($this->request->input('userId'));

        if(!$account)
        {
            return ResponseCodes::UserIdInvalid();
        }

        if(is_null($account->password))
        {
            return ResponseCodes::UserHasNoPassword();
        }

        if(!\Hash::check($this->request->input('oldPassword'), $account->password))
        {
            return ResponseCodes::OldPasswordInvalid();
        }

        /*$passwordReset = PasswordChange::updateOrCreate([
            'user_id' => $this->request->input('userId'),
            'new_password' => \Hash::make($this->request->input('newPassword')),
            'activation_token' => Str::random()
        ]);

        event(new PasswordChangeRequest($passwordReset));*/

        $account->password = \Hash::make($this->request->input('newPassword'));
        $account->save();

        return Response::create(json_encode(true), 200);
    }

    public function postSetPassword()
    {
        $validator = \Validator::make($this->request->all(), [
            'userId' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::find($this->request->input('userId'));

        if(!$account)
        {
            return ResponseCodes::UserIdInvalid();
        }

        $account->password = \Hash::make($this->request->input('password'));
        $account->save();

        return Response::create(json_encode(true), 200);
    }
}