<?php

namespace App\Http\Controllers\Api\v1\Account;

use App\Http\Controllers\Controller;
use App\Libraries\Api\ResponseCodes;
use App\Models\Account\Account;
use App\Models\Brand;
use App\Models\BrandOptin;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class BrandOptinController extends Controller {

    public function postSetOptinStatus(Request $request)
    {
        //account_id
        //brand_id
        //status ( OptIn, OptOut, Neutral )

        $validator = \Validator::make($request->all(), [
            'account_id' => 'required',
            'brand_id' => 'required',
            'status' => 'required|in:OptIn,OptOut,Neutral',
            'source' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::find($request->input('account_id'), ['id']);
        if(!$account)
        {
            return ResponseCodes::UserIdInvalid();
        }

        return $this->doBrandOptin($account, $request->input('brand_id'), $request->input('status'), $request->input('source'));
    }

    public function postSetOptinStatusForEmail(Request $request)
    {
        // email
        // brand_id
        // status ( OptIn, OptOut, Neutral )

        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
            'brand_id' => 'required',
            'status' => 'required|in:OptIn,OptOut,Neutral',
            'source' => 'required'
        ]);

        if($validator->fails())
        {
            return ResponseCodes::InvalidData();
        }

        $account = Account::byEmail($request->input('email'), ['id']);
        if(!$account)
        {
            return ResponseCodes::UserEmailInvalid();
        }

        return $this->doBrandOptin($account, $request->input('brand_id'), $request->input('status'), $request->input('source'));
    }

    private function doBrandOptin($account, $brand_id, $status, $source)
    {
        $brand = Brand::find($brand_id, ['id']);
        if(!$brand)
        {
            return ResponseCodes::BrandNotFound();
        }

        $brandOptin = BrandOptin::firstOrCreate([
            'account_id' => $account->id,
            'brand_id' => $brand->id
        ]);

        if(!$brandOptin->id)
            $brandOptin->id = Uuid::generate(4);
        
        switch ($status)
        {
            case 'OptIn':
            {
                $brandOptin->subscribe_status = 'OptIn';
                $brandOptin->subscribe_source = $source;
                $brandOptin->subscribe_date = date('Y-m-d H:i:s');
            }break;

            case 'OptOut':
            {
                $brandOptin->subscribe_status = 'OptOut';
                $brandOptin->unsubscribe_source = $source;
                $brandOptin->unsubscribe_date = date('Y-m-d H:i:s');
            }break;

            case 'Neutral':
            {
                $brandOptin->subscribe_status = 'Neutral';
            }break;
        }

        $brandOptin->save();

        return $brandOptin->toJson();
    }
}