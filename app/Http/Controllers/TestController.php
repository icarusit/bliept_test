<?php

namespace App\Http\Controllers;

use App\Libraries\SalesForce\Objects\Account;
use App\Libraries\SalesForce\Objects\PickLists\Account\Language;
use App\Libraries\SalesForce\Objects\PickLists\Account\OptIn;
use App\Libraries\SalesForce\Objects\RecordType;
use Illuminate\Http\Request;

class TestController extends Controller {

    public function getIndex()
    {
        $account = new Account();

        $account->FirstName = 'Arnaud';
        $account->LastName = 'Neyt';
        $account->Email = 'arnaud.neyt@studio100.be';
        $account->Street = 'Koekjesstraat 23';
        $account->PostalCode = '2222';
        $account->City = 'Gent';
        $account->Country = 'Belgium';
        $account->OptIn = OptIn::OPT_IN_DOUBLE;
        $account->Language = Language::DUTCH;

        dd($account->create());
    }

    public function getEmail()
    {
        dd(Account::byEmail('arnaud.neyt@studio100.be'));
    }

    public function getExternalId()
    {
        dd(Account::byExternalId(1234));
    }

    public function getAccount()
    {
        $account = Account::byExternalId('6be8ffbd-12be-4b2f-9fcb-e2755dfe383d');
        if(!$account->exists())
            return 'Account does not exist';
        else
            return dd($account);
    }

    public function getTestRedis()
    {
        return dd(\Cache::get('account:0012400000Eergp'));
    }

    public function getRedis()
    {
        \Redis::set('name', 'Stijn Debakker');
        return \Redis::get('name');
    }

    public function getUuid(){
        return \Uuid::generate(4);
    }

    public function getTypeCast()
    {
        $account = Account::byExternalId('9cb420a5-850c-4830-a206-64e50720f94a');
        return $account->getOutputJson();
    }

    public function getDump(Request $request)
    {
        dd($request->all());
    }
}