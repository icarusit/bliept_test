<?php

namespace App\Http\Middleware;

use App\Libraries\Api\ResponseCodes;
use App\Models\Utils\ApiKey;
use App\Models\Utils\ConfigValue;
use Closure;
use Illuminate\Http\Request;

class ApiAuthentication
{
    /**
     * @var Request $request
     */
    private $request;

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @param $requiredRight
     * @return mixed
     */
    public function handle($request, Closure $next, $requiredRight)
    {
        $this->request = $request;

        /*
         * If a locale has not been given, set nl_BE as default
         */

        $this->request->has('locale') ? \Session::put('locale', $this->request->input('locale')) : \Session::put('locale', 'nl_BE');

        if($this->hasApiRights() && $this->hasRequiredRight($requiredRight))
            return $next($request);
        else
            return ResponseCodes::Forbidden();
    }

    public function hasRequiredRight($requiredRight)
    {
        $apiKey = $this->request->input('apiKey');
        $request = $this->request;

        $apiKey = \Cache::remember('system:api_rights:'.$apiKey, 10, function() use ( $request ){
            return ApiKey::whereApiKey($request->input('apiKey'))->first();
        });

        if(!$apiKey)
            return false;

        if($apiKey->full_rights == 1)
            return true;

        if($apiKey->hasRight($requiredRight))
            return true;

        return false;
    }

    /**
     * @return bool
     * @internal param Request $request
     */

    public function hasApiRights()
    {
        $request = $this->request;

        if(!$request->has('apiKey'))
            return false;

        $apiKey = \Cache::remember('system:apikey:'.$request->input('apiKey'), 10, function() use ($request){
           return ApiKey::whereApiKey($request->input('apiKey'))->where('enabled', '=', 1)->first(['ip_adresses']);
        });

        if(!$apiKey)
            return false;

        /*$clientIp = $request->getClientIp();

        if($clientIp != '*' && !strstr($apiKey->ip_adresses, $clientIp))
            return false;*/

        return true;
    }
}
