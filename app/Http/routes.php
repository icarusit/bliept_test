<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controller('test', 'TestController');
Route::controller('auth', 'AuthController');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function(){
    Route::get('/', function(){ return redirect('dashboard'); });

    Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'DashboardController@getIndex']);

    Route::group(['namespace' => 'v1', 'prefix' => 'v1', 'middleware' => 'auth'], function(){
        Route::controller('competitions', 'CompetitionsController');
        Route::controller('api-keys', 'ApiKeysController');
        Route::controller('configuration', 'ConfigurationController');
        Route::controller('ads', 'AdvertentionController');

        Route::group(['namespace' => 'Webshop', 'prefix' => 'webshop', 'middleware' => 'auth'], function(){
            Route::controller('brand-ids', 'BrandIdsController');

            Route::controller('/', 'WebshopController');
        });

        // Brands
        Route::get('brand/list', ['as' => 'admin.brand.list', 'uses' => 'BrandController@getList']);
        Route::get('brand/edit/{id}', ['as' => 'admin.brand.edit', 'uses' => 'BrandController@getEdit']);
        Route::get('brand/new', ['as' => 'admin.brand.new', 'uses' => 'BrandController@getNew']);
        Route::get('brand/delete/{id}', ['as' => 'admin.brand.delete', 'uses' => 'BrandController@getDelete']);
        Route::post('brand/save', ['as' => 'admin.brand.save', 'uses' => 'BrandController@postSave']);

    });
});

Route::group(['namespace' => 'Api', 'prefix' => 'api'], function(){
    Route::group(['namespace' => 'v1', 'prefix' => 'v1'], function(){
        Route::group(['namespace' => 'Account', 'prefix' => 'account', 'middleware' => 'api:account'], function(){

            Route::group(['middleware' => 'api:auth'], function(){
                Route::controller('auth', 'AuthController');
            });

            Route::controller('merge', 'MergeController');

            Route::get('profile/{userId}', 'ProfileController@getIndex');
            Route::post('profile/{userId}', 'ProfileController@postIndex');
            Route::controller('profile', 'ProfileController');

            // Brand Optins
            Route::post('brand-optin/set-optin-status', 'BrandOptinController@postSetOptinStatus');
            Route::post('brand-optin/set-optin-status-for-email', 'BrandOptinController@postSetOptinStatusForEmail');
        });

        // Temp. remove authentication
        Route::group(['namespace' => 'Webshop', 'prefix' => 'webshop'/*, 'middleware' => 'api:webshop'*/], function(){
            Route::controller('products', 'ProductsController');
        });

        Route::group(['middleware' => 'api:scorecard'], function(){
            Route::controller('scorecard', 'ScoreCardController');
        });

        Route::group(['middleware' => 'api:brand'], function(){
            Route::controller('brand', 'BrandController');

        });

        Route::group(['namespace' => 'PubApi', 'prefix' => 'public'], function(){
            Route::controller('ads', 'AdvertentionController');
        });

        Route::group(['middleware' => 'api:mail'], function(){
            Route::controller('mail', 'MailController');
        });

        /*
         * Epg Routes
         */

        Route::group(['prefix' => 'epg'], function(){
            Route::post('two-programs-schedule', 'EpgController@postTwoProgramsSchedule');
            Route::post('schedule-for-day', 'EpgController@postScheduleForDay');
            Route::post('available-days', 'EpgController@postAvailableDays');
            Route::post('junior-programs', 'EpgController@postJuniorPrograms');
            Route::post('studio100tv-programs', 'EpgController@postStudio100tvPrograms');
            Route::post('njamtv-programs', 'EpgController@postNjamtvPrograms');
        });
    });
});

Route::get('/', function(){
    return 'Who are you? Who is this? What are you doing here? Get out :-(';
});