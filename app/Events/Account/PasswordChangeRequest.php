<?php

namespace App\Events\Account;

use App\Events\Event;
use App\Models\Account\PasswordChange;
use Illuminate\Queue\SerializesModels;

class PasswordChangeRequest extends Event
{
    use SerializesModels;

    /**
     * @var PasswordChange
     */
    private $passwordChange;

    /**
     * Create a new event instance.
     * @param PasswordChange $passwordChange
     */
    public function __construct(PasswordChange $passwordChange)
    {
        $this->passwordChange = $passwordChange;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
