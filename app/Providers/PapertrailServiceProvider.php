<?php

namespace App\Providers;

use Illuminate\Log\Writer;
use Illuminate\Support\ServiceProvider;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SyslogHandler;

class PapertrailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if(app('app')->environment() == 'local')
            return;

        $monolog   = app(Writer::class)->getMonolog();
        $syslog    = new SyslogHandler('laravel');
        $formatter = new LineFormatter('%channel%.%level_name%: [BLIEP] %message% %extra%');

        $syslog->setFormatter($formatter);
        $monolog->pushHandler($syslog);
    }
}
