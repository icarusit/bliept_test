<?php

namespace App\Libraries\Api;

use Illuminate\Http\Response;

class ResponseCodes {

    public static function InvalidData()
    {
        return Response::create('INCORRECT_DATA', 400);
    }

    public static function InvalidLogin()
    {
        return Response::create('LOGIN_INVALID', 400);
    }

    public static function AccountInactive()
    {
        return Response::create('ACCOUNT_INACTIVE', 400);
    }

    public static function UserIdInvalid()
    {
        return Response::create('USER_ID_INVALID', 400);
    }

    public static function OldPasswordInvalid()
    {
        return Response::create('OLD_PASSWORD_INVALID', 400);
    }

    public static function Forbidden()
    {
        return Response::create('Forbidden', 401);
    }

    public static function UserEmailInvalid()
    {
        return Response::create('USER_EMAIL_INVALID', 400);
    }

    public static function UserAlreadyExists()
    {
        return Response::create('USER_ALREADY_EXISTS', 400);
    }

    public static function UserAlreadyActivated()
    {
        return Response::create('USER_ALREADY_ACTIVATED', 400);
    }

    public static function ThirdPartyProblem()
    {
        return Response::create('THIRD_PARTY_PROBLEM', 400);
    }

    public static function BrandDoesNotExist()
    {
        return Response::create('BRAND_DOES_NOT_EXIST', 400);
    }

    public static function CampaignDoesNotExist()
    {
        return Response::create('CAMPAIGN_DOES_NOT_EXIST', 400);
    }

    public static function UserHasNoPassword()
    {
        return Response::create('USER_HAS_NO_PASSWORD', 400);
    }

    public static function NothingFound()
    {
        return Response::create('NOTHING_FOUND', 400);
    }

    public static function BrandNotFound()
    {
        return Response::create('BRAND_NOT_FOUND', 400);
    }
}