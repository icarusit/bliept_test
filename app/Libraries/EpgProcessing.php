<?php

namespace App\Libraries;

use App\Models\Epg\JuniorTVBroadcast;
use App\Models\Epg\JuniorTVProgram;
use App\Models\Epg\NjamTV;
use App\Models\Epg\Studio100TV;
use DateTime;
use DateTimeZone;
use DB;
use Log;
use FTP;
use Sabre\Xml\Reader;

class EpgProcessing {

    public static function processStudio100TV()
    {
        try
        {
            $table = new Studio100TV();
            $table = $table->getTable();

            date_default_timezone_set('Europe/Brussels');
            setlocale(LC_ALL, 'nl_NL');

            Log::alert('[EPG] Fetching file from FTP Server...');

            $connection = FTP::connection('studio100tv');
            $connection->changeDir('Magnet');

            $files = $connection->getDirListing();
            natsort($files);

            $remoteFile = $files[count($files)-1];

            $localFile = public_path('epg_'.time().'.xml');
            $connection->downloadFile($remoteFile, $localFile);

            Log::alert('[EPG] File stored locally. Truncating the database table.');
            DB::table($table)->truncate();
            Log::alert('[EPG] Database Truncated! Start processing new data');

            $reader = new Reader();
            $reader->elementMap = [
                '{}program' => 'Sabre\Xml\Element\KeyValue'
            ];
            $reader->xml(file_get_contents($localFile));

            $programs = $reader->parse();
            foreach($programs['value'] as $program)
            {
                $values = $program['value'];

                Studio100TV::create([
                    'program_id' => $program['attributes']['programma_id'],
                    'day_name' => date('D', strtotime($values['{}programma_datum'])),
                    'program_date' => date('Y-m-d', strtotime($values['{}programma_datum'])),
                    'onscreen_date' => date('Y-m-d', strtotime($values['{}uitzend_datum'])),
                    'start_time' => date('Y-m-d', strtotime($values['{}uitzend_datum'])) . ' ' . $values['{}beginuur'],
                    'end_time' => date('Y-m-d', strtotime($values['{}uitzend_datum'])) . ' ' . $values['{}einduur'],
                    'length' => '0000-00-00 '.$values['{}duur'],
                    'show_title' => $values['{}serie_titel'],
                    'show_year' => $values['{}jaar'],
                    'country' => $values['{}land'],
                    'episode_title' => $values['{}afleverings_titel'],
                    'episode_number' => $values['{}afleveringsnummer'],
                    'episode_count' => $values['{}aantal_afleveringen'],
                    'jaargang' => $values['{}jaargang'],
                    'cast' => $values['{}cast'],
                    'synopsis' => $values['{}synopsis'],
                    'category' => $values['{}category'],
                    'tn_replay_rights' => ($values['{}telenet_replay'][0]['value'] == 'YES') ? 1:0,
                    'tn_replay_days' => $values['{}telenet_replay'][1]['value']
                ]);

            }
        } catch ( Exception $e )
        {
            Log::error($e);
            return $e;
        }
    }

    public static function processNjamTV()
    {
        try
        {
            $table = new NjamTV();
            $table = $table->getTable();

            date_default_timezone_set('Europe/Brussels');
            setlocale(LC_ALL, 'nl_NL');

            Log::alert('[EPG] Fetching file from FTP Server...');

            $connection = FTP::connection('njamtv');
            $connection->changeDir('Magnet');

            $files = $connection->getDirListing();
            natsort($files);

            $remoteFile = $files[count($files)-1];

            $localFile = public_path('epg_'.time().'.xml');
            $connection->downloadFile($remoteFile, $localFile);

            Log::alert('[EPG] File stored locally. Truncating the database table.');
            DB::table($table)->truncate();
            Log::alert('[EPG] Database Truncated! Start processing new data');

            $reader = new Reader();
            $reader->elementMap = [
                '{}program' => 'Sabre\Xml\Element\KeyValue'
            ];
            $reader->xml(file_get_contents($localFile));

            $programs = $reader->parse();
            foreach($programs['value'] as $program)
            {
                $values = $program['value'];

                NjamTV::create([
                    'program_id' => $program['attributes']['programma_id'],
                    'day_name' => date('D', strtotime($values['{}programma_datum'])),
                    'program_date' => date('Y-m-d', strtotime($values['{}programma_datum'])),
                    'onscreen_date' => date('Y-m-d', strtotime($values['{}uitzend_datum'])),
                    'start_time' => date('Y-m-d', strtotime($values['{}uitzend_datum'])) . ' ' . $values['{}beginuur'],
                    'end_time' => date('Y-m-d', strtotime($values['{}uitzend_datum'])) . ' ' . $values['{}einduur'],
                    'length' => '0000-00-00 '.$values['{}duur'],
                    'show_title' => $values['{}serie_titel'],
                    'show_year' => $values['{}jaar'],
                    'country' => $values['{}land'],
                    'episode_title' => $values['{}afleverings_titel'],
                    'episode_number' => $values['{}afleveringsnummer'],
                    'episode_count' => $values['{}aantal_afleveringen'],
                    'jaargang' => $values['{}jaargang'],
                    'cast' => $values['{}cast'],
                    'synopsis' => $values['{}synopsis'],
                    'tn_replay_rights' => ($values['{}telenet_replay'][0]['value'] == 'YES') ? 1:0,
                    'tn_replay_days' => $values['{}telenet_replay'][1]['value']
                ]);

            }
        } catch ( Exception $e )
        {
            Log::error($e);
            return $e;
        }
    }

    public static function processJuniorTV()
    {
        try {

            $programTable = new JuniorTVProgram();
            $programTable = $programTable->getTable();

            $broadcastTable = new JuniorTVBroadcast();
            $broadcastTable = $broadcastTable->getTable();

            date_default_timezone_set('Europe/Brussels');
            setlocale(LC_ALL, 'nl_NL');

            Log::alert('[EPG] Fetching file from FTP Server...');

            $connection = FTP::connection('juniortv');
            $dirListing = $connection->getDirListing();

            $xmls = array();

            foreach($dirListing as $listing)
            {
                if(strstr($listing, '.xml'))
                    array_push($xmls, $listing);
            }

            rsort($xmls);
            $remoteFile = $xmls[0];

            $localFile = public_path('epg_junior_'.time().'.xml');
            $connection->downloadFile($remoteFile, $localFile);

            Log::alert('[EPG][JUNIOR] File stored locally. Truncating the database tables');
            DB::table($broadcastTable)->truncate();
            DB::table($programTable)->truncate();
            Log::alert('[EPG][JUNIOR] Database Truncated! Start processing new data!');

            $reader = new Reader();
            $reader->elementMap = [
                '{}Program_element' => 'Sabre\Xml\Element\KeyValue'
            ];

            if(!mb_check_encoding($localFile, 'UTF-8'))
                mb_convert_encoding($localFile, 'UTF-8');

            $reader->xml(utf8_encode(file_get_contents($localFile)));

            $fullFile = $reader->parse();

            foreach($fullFile['value'] as $value)
            {
                if($value['name'] != '{}Program_element')
                    continue;

                JuniorTVProgram::updateOrCreate([
                    'program_reference_number' => $value['value']['{}program_reference_number'],
                ], [
                    'program_reference_number' => $value['value']['{}program_reference_number'],
                    'series_reference_number' => (isset($value['value']['{}series_reference_number'])) ? $value['value']['{}series_reference_number'] : '',
                    'program_type' => (isset($value['value']['{}programtype'])) ? $value['value']['{}programtype'] : '',
                    'title' => (isset($value['value']['{}title'])) ? $value['value']['{}title'] : '',
                    'original_title' => (isset($value['value']['{}original_title'])) ?  $value['value']['{}original_title'] : '',
                    'episode_title' => (isset($value['value']['{}episode_title'])) ? $value['value']['{}episode_title'] : '',
                    'original_episode_title' => (isset($value['value']['{}original_episode_title'])) ? $value['value']['{}original_episode_title'] : '',
                    'episode_index' => (isset($value['value']['{}episode_index'])) ? $value['value']['{}episode_index'] : '',
                    'season' => (isset($value['value']['{}season'])) ? $value['value']['{}season'] : '',
                    'genre' => (isset($value['value']['{}genre'])) ? $value['value']['{}genre'] : '',
                    'production_countries' => (isset($value['value']['{}production_countries'])) ? $value['value']['{}production_countries'] : '',
                    'production_years' => (isset($value['value']['{}production_years'])) ? $value['value']['{}production_years'] : '',
                    'aspect_ratio' => (isset($value['value']['{}aspect_ratio'])) ? $value['value']['{}aspect_ratio'] : '',
                    'description_short' => (isset($value['value']['{}description_short'])) ? $value['value']['{}description_short'] : '',
                    'resource' => (isset($value['value']['{}resource'])) ? $value['value']['{}resource'] : ''
                ]);
            }

            Log::alert('[EPG][JUNIOR] Program database synched with Sky.');
            Log::alert('[EPG][JUNIOR] Synching broadcast schedule.');

            $reader->elementMap = [
                '{}eventdetails' => 'Sabre\Xml\Element\KeyValue'
            ];
            $reader->xml(file_get_contents($localFile));

            $fullFile = $reader->parse();

            foreach($fullFile['value'] as $value)
            {
                if($value['name'] != '{}Broadcast_linear')
                    continue;

                if(!array_key_exists(2, $value['value']))
                    continue;

                $event_id = $value['value'][1]['value'];
                $program = $value['value'][2]['value'];

                //dd($program);

                if(!isset($program['{}program_reference_number']))
                    continue;

                $displayedTimeStart = new DateTime(date('Y-m-d H:i:s', strtotime($program['{}displayed_time_start'])), new DateTimeZone('GMT'));
                $displayedTimeStart->setTimezone(new DateTimeZone('Europe/Brussels'));

                $displayedTimeEnd = new DateTime(date('Y-m-d H:i:s', strtotime($program['{}displayed_time_end'])), new DateTimeZone('GMT'));
                $displayedTimeEnd->setTimezone(new DateTimeZone('Europe/Brussels'));

                $realTimeStart = new DateTime(date('Y-m-d H:i:s', strtotime($program['{}sig_time_start'])), new DateTimeZone('GMT'));
                $realTimeStart->setTimezone(new DateTimeZone('Europe/Brussels'));

                $realTimeEnd = new DateTime(date('Y-m-d H:i:s', strtotime($program['{}sig_time_end'])), new DateTimeZone('GMT'));
                $realTimeEnd->setTimezone(new DateTimeZone('Europe/Brussels'));

                JuniorTVBroadcast::updateOrCreate([
                    'event_id' => $event_id,
                    'program_reference_number' => $program['{}program_reference_number']
                ], [
                    'event_id' => $event_id,
                    'program_reference_number' => $program['{}program_reference_number'],
                    'displayed_time_start' => $displayedTimeStart,
                    'displayed_time_end' => $displayedTimeEnd,
                    'real_time_start' => $realTimeStart,
                    'real_time_end' => $realTimeEnd,
                    'parental_rating' => (isset($program['{}parental_rating'])) ? $program['{}parental_rating'] : '',
                    'aspect_ratio' => (isset($program['{}aspect_ratio'])) ? $program['{}aspect_ratio'] : '',
                    'duration' => (isset($program['{}duration'])) ? $program['{}duration'] : '00:00:00',
                    'first_episode' => (isset($program['{}first_episode'])) ? $program['{}first_episode'] : false,
                    'pilot' => (isset($program['{}pilot'])) ? $program['{}pilot'] : false,
                    'live' => (isset($program['{}live'])) ? $program['{}live'] : false,
                    'tv_premiere' => (isset($program['{}tvpremiere'])) ? $program['{}tvpremiere'] : false,
                    'hd_premiere' => (isset($program['{}hdpremiere'])) ? $program['{}hdpremiere'] : false,
                ]);
            }

        } catch ( Exception $e )
        {
            Log::error($e);
        }
    }

}

