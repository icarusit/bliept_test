<?php namespace App\Libraries;

class Helper {

    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = \DateTime::createFromFormat($format, $date);

        $isValid = $d && $d->format($format) == $date;

        return $isValid && $date >= '1900-01-01';
    }

    /**
     * @param $string
     * @return int
     */

    public static function isValidUuid4($string)
    {
        return preg_match('^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$^', $string);
    }

    public static function is_json($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}