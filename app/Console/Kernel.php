<?php

namespace App\Console;

use App\Console\Commands\DeployCommands;
use App\Console\Commands\ImportEpgFiles;
use App\Console\Commands\Inspire;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Inspire::class,
        ImportEpgFiles::class,
        DeployCommands::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('epg:import')
            ->daily();
    }
}
