<?php

namespace App\Console\Commands;

use App\Libraries\EpgProcessing;
use Illuminate\Console\Command;

class ImportEpgFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'epg:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Studio 100 TV and Junior TV EPG Files';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Starting processing Studio 100 TV EPG");
        EpgProcessing::processStudio100TV();
        $this->info("Studio 100 TV EPG has been processed. Starting processing JuniorTV EPG");
        EpgProcessing::processJuniorTV();
        $this->info("JuniorTV EPG has been processed. Starting NjamTV EPG");
        EpgProcessing::processNjamTV();
        $this->info("All done sharky");
    }   
}
