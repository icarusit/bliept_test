<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class DeployCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy:initial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initial command to run on deploy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setupPapertrail();
    }
    
    private function setupPapertrail()
    {
        if(env('APP_ENV') == 'staging')
        {
            $process = new Process('sed -i -e \'1i$LocalHostName bliep-staging.studio100.com\\\' /etc/rsyslog.conf');
            $process->run();
        }
        elseif(env('APP_ENV') == 'live')
        {
            $process = new Process('sed -i -e \'1i$LocalHostName bliep.studio100.com\\\' /etc/rsyslog.conf');
            $process->run();
        }
        $process = new Process('sudo echo "*.* @@logs3.papertrailapp.com:48087" >> /etc/rsyslog.conf');
        $process->run();

        $process = new Process('sudo service rsyslog restart');
        $process->run();
    }
}
