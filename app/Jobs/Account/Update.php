<?php

namespace App\Jobs\Account;

use App\Jobs\Job;
use App\Libraries\SalesForce\Objects\Account;
use Illuminate\Contracts\Bus\SelfHandling;

class Update extends Job implements SelfHandling
{
    private $account;

    /**
     * Create a new job instance.
     *
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
