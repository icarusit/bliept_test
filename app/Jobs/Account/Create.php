<?php

namespace App\Jobs\Account;

use App\Jobs\Job;
use App\Libraries\SalesForce\Objects\Account;
use Illuminate\Contracts\Bus\SelfHandling;

class Create extends Job implements SelfHandling
{
    private $fieldValues;

    /**
     * Create a new job instance.
     *
     * @param $fieldValues
     */
    public function __construct($fieldValues)
    {
        $this->fieldValues = $fieldValues;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = \Salesforce::create( [$this->fieldValues] , 'Account');

        if($account[0]->success)
        {
            /*
             * Retrieve and cache the account for further use.
             */

            $account = Account::byExternalId($this->fieldValues['External_Id__c']);

            \Log::alert('[QUEUE][ACCOUNT] Account creation succeeded: '.$account->ExternalId);
        }
        else
        {
            \Log::error('[QUEUE][ACCOUNT] Account creation failed: '.$account[0]->errors[0]->message);
        }
    }
}
