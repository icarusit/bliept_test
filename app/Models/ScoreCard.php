<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreCard extends Model {

    protected $table = 'scorecards';
    protected $guarded = ['updated_at', 'created_at'];

}