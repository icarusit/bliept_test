<?php

namespace App\Models\Account;

use App\Models\Brand;
use App\Models\BrandOptin;
use Illuminate\Database\Eloquent\Model;

class Account extends Model {

	protected $table = 'accounts';

	protected $guarded = ['created_at', 'updated_at'];

	public function brandOptins()
	{
		return $this->hasMany('\App\Models\BrandOptin', 'account_id');
	}

	public function OptinnedBrands()
	{
		return $this->hasManyThrough(Brand::class, 'brand_optins', 'account_id', 'brand_id')
			->withPivot('subscribe_status', 'subscribe_source', 'subscribe_date', 'unsubscribe_source', 'unsubscribe_date');
	}

	public static function byEmail($email, $columns = ['*'])
	{
		return self::where('email', '=', $email)->first($columns);
	}

	public static function emailExists($email)
	{
		$account = self::where('email', '=', $email)->first(['id']);
		return ($account) ? true:false;
	}
}