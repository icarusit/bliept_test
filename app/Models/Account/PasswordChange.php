<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

class PasswordChange extends Model {

    protected $table = 'password_changes';

}
