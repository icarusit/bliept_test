<?php

namespace App\Models\Utils;

use Illuminate\Database\Eloquent\Model;

class ApiRight extends Model {

    protected $table = 'api_rights';

    protected $guarded = ['updated_at', 'created_at'];

}