<?php

namespace App\Models\Utils;

use Illuminate\Database\Eloquent\Model;

class ConfigValue extends Model {

    protected $table = 'configuration';
    protected $guarded = ['created_at', 'updated_at'];

    public static function get($systemName, $default='')
    {
        return \Cache::remember('config:get:'.$systemName, 10, function() use ( $systemName ) {
            return self::where('system_name', '=', $systemName)->first()->value;
        });
    }

    public static function set($systemName, $value, $friendlyName=null)
    {
        \Cache::forget('config:has:'.$systemName);
        \Cache::forget('config:get:'.$systemName);

        $data = ['value' => $value];

        if(!is_null($friendlyName))
            $data['friendly_name'] = $friendlyName;

        return self::updateOrCreate(['system_name' => $systemName], $data);
    }

    public static function has($systemName)
    {
        return \Cache::remember('config:has:'.$systemName, 10, function() use ( $systemName ){
            return self::where('system_name', '=', $systemName)->first(['system_name']) ? true : false;
        });

    }

}