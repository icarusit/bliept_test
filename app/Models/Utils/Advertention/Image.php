<?php

namespace App\Models\Utils\Advertention;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

	protected $table = 'advertention_images';
	protected $guarded = ['created_at', 'updated_at'];

	public function campaign()
	{
		return $this->belongsTo('\App\Models\Utils\Advertention\Campaign', 'id', 'campaign_id');
	}
}