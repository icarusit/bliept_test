<?php

namespace App\Models\Utils\Advertention;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model {

	protected $table = 'advertention_campaigns';
	protected $guarded = ['created_at', 'updated_at'];

	public function images()
	{
		return $this->hasMany('\App\Models\Utils\Advertention\Image', 'campaign_id', 'id');
	}

}