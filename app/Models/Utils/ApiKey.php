<?php

namespace App\Models\Utils;

use App\Models\Utils\ApiRight;
use Illuminate\Database\Eloquent\Model;

class ApiKey extends Model {

    protected $table = 'api_keys';

    protected $guarded = ['created_at', 'updated_at'];

    public function hasRight($rightName)
    {
        $apiRight = ApiRight::where('api_key_id', '=', $this->id)
            ->where('api_right', '=', $rightName)->first();

        return ($apiRight) ? true:false;
    }

    public function addRight($rightName)
    {
        $apiRight = ApiRight::where('api_key_id', '=', $this->id)
            ->where('api_right', '=', $rightName)->first();

        if(!$apiRight)
        {
            ApiRight::create([
                'api_key_id' => $this->id,
                'api_right' => $rightName
            ]);
        }
    }

    public function removeRight($rightName)
    {
        ApiRight::where('api_key_id', '=', $this->id)
            ->where('api_right', '=', $rightName)->delete();
    }
}