<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandOptin extends Model {

	protected $table = 'brand_optins';

	protected $guarded = ['created_at', 'updated_at'];

	public function brand()
	{
		return $this->belongsTo('\App\Models\Brand');
	}

}