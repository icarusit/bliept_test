<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model {

	protected $table = 'brands';

	protected $guarded = ['created_at', 'updated_at'];

	public function parent()
	{
		return $this->belongsTo('\App\Models\Brand', 'parent_brand_id');
	}

	public function optinAccounts()
	{
		return $this->hasManyThrough('App\Models\Account\Account', 'App\Models\BrandOptin', 'brand_id', 'id');
	}

	public static function getActiveBrands()
	{
		return self::where('active', '=', 1)->get();
	}

}