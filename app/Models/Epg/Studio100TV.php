<?php

namespace App\Models\Epg;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Studio100TV extends Model {

    use Rememberable;
    
    protected $guarded = [ 'created_at', 'updated_at' ];

    protected $table = 'epg_studio100tv';

}