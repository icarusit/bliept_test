<?php

namespace App\Models\Lookups;

use Illuminate\Database\Eloquent\Model;

class WebshopBrandId extends Model {

    protected $table = 'webshop_brand_ids';

    protected $guarded = ['created_at', 'updated_at'];

    public static function getWebshopBrandID($externalId)
    {
        $obj = self::where('sf_brand_id', '=', $externalId)->first();
        if(!$obj)
            return false;

        return $obj->webshop_brand_id;
    }
}