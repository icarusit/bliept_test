<?php

namespace App\Listeners\Account;

use App\Events\Account\PasswordChangeRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPasswordChangeEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PasswordChangeRequest  $event
     * @return void
     */
    public function handle(PasswordChangeRequest $event)
    {
        // TODO: send the actual email
    }
}
