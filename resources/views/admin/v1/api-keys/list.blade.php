@extends('admin.template')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="panel">
                <div class="panel-heading">
                   <span class="panel-title">
                       API Keys
                   </span>
                </div>

                <div class="panel-body">

                    <div class="col-md-12">
                        <div class="">
                            <a href="{{ url('admin/v1/api-keys/new') }}" class="btn btn-primary">+ Add</a>
                        </div>
                    </div>

                    <table class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Description</th>
                                <th>Full Rights</th>
                                <th>Enabled</th>
                                <th>IP Adresses</th>
                                <th>Key</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($keys as $key)

                                <tr>
                                    <td>{{ $key->description }}</td>
                                    <td>{{ $key->full_rights ? 'Yes':'No' }}</td>
                                    <td>{{ $key->enabled ? 'Yes':'No' }}</td>
                                    <td>{{ $key->ip_adresses }}</td>
                                    <td key-id="{{ $key->id }}">*****</td>
                                    <td>
                                        <div class="btn-group">
                                            <a id="key" href="#" key-id="{{ $key->id }}" class="btn btn-xs btn-default">View</a>
                                            <a href="{{ url('admin/v1/api-keys/edit/'.$key->id) }}" class="btn btn-xs btn-default">Edit</a>
                                            <a href="{{ url('admin/v1/api-keys/delete/'.$key->id) }}" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure?')) return false;">Delete</a>                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@stop

@section('scripts')
    <script>
        $(document).ready(function(){

            var baseUrl = '{{ url('admin/v1/api-keys') }}';

            $('a#key').click(function()
            {

                var keyId = $(this).attr('key-id');

                if($(this).text() != 'Hide')
                {
                    $(this).text('Loading...');
                    $.post(baseUrl + '/get-key', { keyId: keyId, _token: '{{ csrf_token() }}' }, function(data){
                        $('td[key-id='+keyId+']').html(data);

                        $('a#key').text('Hide');
                    });
                }
                else
                {
                    $('td[key-id='+keyId+']').text('*****');
                    $(this).text('View');
                }
            });

        });
    </script>
@stop