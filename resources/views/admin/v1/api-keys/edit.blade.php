@extends('admin.template')

@section('content')

    <div class="row">
        <form class="form-horizontal" method="post" action="{{ url('admin/v1/api-keys/save') }}">
            <div class="col-md-9">

                <div class="panel">
                    <div class="panel-heading">
                       <span class="panel-title">
                           Edit API Key
                       </span>
                    </div>

                    <div class="panel-body">

                        <input type="hidden" name="id" value="{{ $key->id }}"/>

                        <div class="form-group">
                            <label class="control-label col-lg-3">API Key</label>

                            <div class="col-md-8">
                                <input type="text" class="form-control" name="api_key" readonly value="{{ $key->api_key }}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">IP Addresses</label>

                            <div class="col-md-8">
                                <textarea name="ip_adresses" class="form-control">{{ $key->ip_adresses }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Description</label>

                            <div class="col-md-8">
                                <input type="text" name="description" class="form-control" value="{{ $key->description }}"/>
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer">
                        <div class="btn-group">
                            <input type="submit" name="submit" value="Save" class="btn btn-primary" />
                        </div>

                        <div class="pull-right">
                            <a href="{{ url('admin/v1/api-keys/list') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">API Rights</span>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="checkbox-custom mb5">
                                <input type="checkbox" id="full_rights" name="full_rights" @if($key->full_rights) checked @endif>
                                <label for="full_rights">Full Rights</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="checkbox-custom mb5">
                                <input type="checkbox" id="auth" name="auth" @if($key->hasRight('auth')) checked @endif>
                                <label for="auth">Authentication</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="checkbox-custom mb5">
                                <input type="checkbox" id="account" name="account" @if($key->hasRight('account')) checked @endif>
                                <label for="account">Account</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="checkbox-custom mb5">
                                <input type="checkbox" id="webshop" name="webshop" @if($key->hasRight('webshop')) checked @endif>
                                <label for="webshop">Webshop</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="checkbox-custom mb5">
                                <input type="checkbox" id="scorecard" name="scorecard" @if($key->hasRight('scorecard')) checked @endif>
                                <label for="scorecard">Scorecard</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="checkbox-custom mb5">
                                <input type="checkbox" id="mail" name="mail" @if($key->hasRight('mail')) checked @endif>
                                <label for="mail">Mail</label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

@stop