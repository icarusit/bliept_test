@extends('admin.template')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel">
                <div class="panel-heading">
                   <span class="panel-title">
                       Competitions
                   </span>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Internal Code</th>
                                <th>Quantity</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($competitions as $competition)
                                <tr>
                                    <td>{{ $competition->cmp_form_name }}</td>
                                    <td>{{ $competition->cmp_form_internal_code }}</td>
                                    <td>{{ $competition->qty }}</td>
                                    <td>
                                        <a href="{{ url('admin/v1/competitions/export/'.$competition->cmp_form_internal_code) }}">
                                            Export
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@stop