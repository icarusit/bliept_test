@extends('admin.template')

@section('content')

    <div class="row">

        <div class="col-md-12">

            <div class="panel">
                <div class="panel-heading">
                   <span class="panel-title">
                       Configuration Values
                   </span>
                </div>

                <div class="panel-body">

                    <div class="col-md-12">
                        <div class="">
                            <a href="{{ url('admin/v1/configuration/add') }}" class="btn btn-primary">+ Add</a>
                            <a href="{{ url('admin/v1/configuration/flush-cache') }}" class="btn btn-default">Flush Cache</a>
                        </div>
                    </div>

                    <table class="table table-striped table-responsive">
                        <thead>
                            <tr>
                                <th>Friendly Name</th>
                                <th>System Name</th>
                                <th>Value</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($configs as $config)

                                <tr>
                                    <td>{{ $config->friendly_name }}</td>
                                    <td>{{ $config->system_name }}</td>
                                    <td>{{ $config->value }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ url('admin/v1/configuration/edit/'.$config->id) }}" class="btn btn-xs btn-default">Edit</a>
                                            <a href="{{ url('admin/v1/configuration/delete/'.$config->id) }}" class="btn btn-xs btn-danger" onclick="if(!confirm('Are you sure?')) return false;">Delete</a>                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@stop
