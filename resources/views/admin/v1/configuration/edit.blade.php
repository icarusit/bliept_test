@extends('admin.template')

@section('content')

    <div class="row">
        <form class="form-horizontal" method="post" action="{{ url('admin/v1/configuration/save') }}">
            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-heading">
                       <span class="panel-title">
                           Edit Configuration Value
                       </span>
                    </div>

                    <div class="panel-body">

                        <input type="hidden" name="id" value="{{ $config->id }}"/>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Friendly Name</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="friendly_name" value="{{ $config->friendly_name }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">System Name</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="system_name" value="{{ $config->system_name }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Value</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="value" value="{{ $config->value }}" />
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <div class="btn-group">
                            <input type="submit" name="submit" value="Save" class="btn btn-primary" />
                        </div>

                        <div class="pull-right">
                            <a href="{{ url('admin/v1/configuration/list') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>

@stop