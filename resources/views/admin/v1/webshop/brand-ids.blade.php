@extends('admin.template')

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Webshop Brand Ids <-> SF Brand Ids</span>
                </div>

                <div class="panel-body">

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Webshop #</th>
                                <th>SF #</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($brandIds as $brandid)
                                <tr>
                                    <td>{{ $brandid->name }}</td>
                                    <td>{{ $brandid->webshop_brand_id }}</td>
                                    <td>{{ $brandid->sf_brand_id }}</td>
                                    <td>
                                        <a href="{{ url('admin/v1/webshop/brand-ids/delete/'.$brandid->id) }}" class="btn btn-xs btn-danger"
                                            onclick="if(!confirm('Are you sure?')) return false;">
                                            <i class="glyphicons glyphicons-remove"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Create new</span>
                </div>

                <div class="panel-body">
                    <form action="{{ url('admin/v1/webshop/brand-ids/save') }}" method="post">

                        <div class="form-horizontal">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Webshop ID*" name="webshop_brand_id" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="SalesForce ID*" name="sf_brand_id" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Brand Name*" name="name" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" name="submit" class="btn btn-primary">Create!</button>
                                </div>
                            </div>

                        </div>

                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop