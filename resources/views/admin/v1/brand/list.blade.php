@extends('admin.template')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">

                    <div class="pull-right">
                        <a href="{{ route('admin.brand.new') }}" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Create new brand</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="panel">
                <div class="panel-heading">
                   <span class="panel-title">
                       Brands
                   </span>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-responsive">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Active</th>
                            <th>Web Visits</th>
                            <th>Subscribers</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($brands as $brand)
                            <tr>
                                <td>{{ $brand->id }}</td>
                                <td>{{ $brand->name }}</td>
                                <td>{{ $brand->active ? 'Yes' : 'No' }}</td>
                                <td>{{ $brand->web_visits }}</td>
                                <td>{{ $brand->brand_subscribes }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.brand.edit', ['id' => $brand->id]) }}" class="btn btn-sm btn-default">Edit</a>
                                        <a href="{{ route('admin.brand.delete', ['id' => $brand->id]) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@stop