@extends('admin.template')


@section('content')
    <form class="form-horizontal" action="{{ route('admin.brand.save') }}" method="post">

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">

                        <div class="pull-right">
                            <a href="{{ route('admin.brand.list') }}" class="btn btn-default">Back</a>
                            <button type="submit" name="submit" class="btn btn-primary">Save</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">Edit Brand</span>
                    </div>

                    <div class="panel-body">

                        <input type="hidden" name="id" value="{{ $brand->id }}" />

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Name</label>
                            <div class="col-md-9">
                                <input class="form-control" name="name" value="{{ $brand->name ?: Input::old('name') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Description</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description">{{ $brand->description ?: Input::old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Thumbnail url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="thumbnail_url" value="{{ $brand->thumbnail_url ?: Input::old('thumbnail_url') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Facebook url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="facebook_url" value="{{ $brand->facebook_url ?: Input::old('facebook_url') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Twitter url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="twitter_url" value="{{ $brand->twitter_url ?: Input::old('twitter_url') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Instagram url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="instagram_url" value="{{ $brand->instagram_url ?: Input::old('instagram_url') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Youtube url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="youtube_url" value="{{ $brand->youtube_url ?: Input::old('youtube_url') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Web url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="web_url" value="{{ $brand->web_url ?: Input::old('web_url') }}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Webshop category url</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="webshop_category_url" value="{{ $brand->webshop_category_url ?: Input::old('webshop_category_url') }}" />
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">

                        <div class="pull-right">
                            <a href="{{ route('admin.brand.list') }}" class="btn btn-default">Back</a>
                            <button type="submit" name="submit" class="btn btn-primary">Save</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        {{ csrf_field() }}

    </form>

@stop