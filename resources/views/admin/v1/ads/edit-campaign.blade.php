@extends('admin.template')

@section('content')

    <form action="{{ url('admin/v1/ads/save-campaign') }}" method="post">

        <input type="hidden" name="id" value="{{ $campaign->id }}" />

        <div class="row">
            <div class="col-md-10">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">Create Campaign</span>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="control-label col-lg-2">Title</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="title" value="{{ $campaign->title }}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Description</label>
                                <div class="col-md-9">
                                    <textarea name="description" class="form-control">{{ $campaign->description }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Customer</label>
                                <div class="col-md-9">
                                    <input type="text" name="customer" class="form-control" value="{{ $campaign->customer }}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Display Website Url</label>
                                <div class="col-md-9">
                                    <input type="text" name="website_url" class="form-control" value="{{ $campaign->website_url }}" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">Toolbox</span>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <input type="checkbox" name="active" @if($campaign->active) checked @endif  />
                                </div>
                                <label class="col-lg-8">Campaign Active?</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="panel">
                    <div class="panel-body">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary" style="width: 100%;">Save</button>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('admin/v1/ads') }}" class="btn btn-default" style="width: 100%">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

@stop