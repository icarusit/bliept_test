@extends('admin.template')

@section('content')

    <form action="{{ url('admin/v1/ads/save-image') }}" method="post">

        <input type="hidden" name="id" value="{{ $image->id }}" />
        <input type="hidden" name="campaign_id" value="{{ $image->campaign_id }}" />

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ url('admin/v1/ads/view-campaign/'.$image->campaign_id) }}" class="btn btn-default">Terug</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">Edit image</span>
                    </div>

                    <div class="panel-body">

                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="control-label col-lg-1">Image URL</label>
                                <div class="col-md-11">
                                    <input type="text" name="image_url" class="form-control" value="{{ $image->image_url }}" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-1">Redirect URL</label>
                                <div class="col-md-11">
                                    <input type="text" name="website_url" class="form-control" value="{{ $image->website_url }}" />
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>

@stop