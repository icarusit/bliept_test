@extends('admin.template')

@section('content')



    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <a href="{{ url('admin/v1/ads/new-image/'.$campaign_id) }}" class="btn btn-primary"><i class="fa fa-plus"></i> New image</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Images</span>
                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Preview</th>
                                <th>Encoded URL</th>
                                <th>Impressions</th>
                                <th>CTR</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($campaign->images as $image)
                                <tr>
                                    <td><img src="{{ $image->image_url }}" height="150" /></td>
                                    <td><strong>Image:</strong> {{ $image->encoded_image_url }}<br /><strong>Redirect:</strong> {{ $image->encoded_website_url }}</td>
                                    <td>{{ $image->impressions }}</td>
                                    <td>{{ ($image->clicks) > 0 ? number_format($image->clicks / $image->impressions * 100, 2) : 0 }}%</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ url('admin/v1/ads/edit-image/'.$image->id) }}" class="btn btn-default"><i class="glyphicons glyphicons-edit"></i> Edit</a>
                                            <a href="{{ url('admin/v1/ads/delete-image/'.$image->id) }}" class="btn btn-danger"><i class="glyphicons glyphicons-remove"></i> Remove</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop