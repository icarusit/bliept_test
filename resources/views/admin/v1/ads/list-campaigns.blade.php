@extends('admin.template')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <a href="{{ url('admin/v1/ads/create-campaign') }}"
                        class="btn btn-primary">Create Campaign</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Advertentions</span>
                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Active</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($campaigns as $campaign)
                                <tr>
                                    <td>{{ $campaign->title }}</td>
                                    <td>{{ $campaign->active ? 'Yes':'No' }}</td>
                                    <td>
                                        <div class="btn-group-xs">
                                            <a href="{{ url('admin/v1/ads/view-campaign/'.$campaign->id) }}"
                                                class="btn btn-xs btn-primary">Select</a>
                                            <a href="{{ url('admin/v1/ads/edit-campaign/'.$campaign->id) }}"
                                               class="btn btn-xs btn-default">Edit</a>
                                            <a href="{{ url('admin/v1/ads/delete-campaign/'.$campaign->id) }}" onclick="if(!confirm('Bent u zeker?')) return false;"
                                               class="btn btn-xs btn-warning">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop