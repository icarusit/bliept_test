<?php if(!isset($sitetitle)) $sitetitle = ''; ?>
        <!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Studio 100 - Bliep - {{ $sitetitle }}</title>
    <meta name="author" content="Stijn Debakker">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/skin/default_skin/css/theme.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/fonts/glyphicons-pro/glyphicons-pro.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/fonts/octicons/octicons.css') }}">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/custom.css') }}" />

    @yield('styles')

</head>

<body class="blank-page">

<!-- Start: Main -->
<div id="main">

    <!-- Start: Header -->
    <header class="navbar navbar-fixed-top">
        <div class="navbar-branding">
            <a class="navbar-brand" href="{{ url('admin/dashboard') }}">
                <img src="{{ asset('logo.png') }}" width="160" />
            </a>
            <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
        </div>
        <form class="navbar-form navbar-left navbar-search" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search...">
            </div>
        </form>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown"> <img src="{{ Gravatar::get(Auth::user()->email) }}" alt="avatar" class="mw30 br64 mr15"> {{ Auth::user()->name }}
                    <span class="caret caret-tp hidden-xs"></span>
                </a>
                <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
                    <li class="list-group-item">
                        <a href="{{ url('portal/account/billing') }}" class="animated animated-short fadeInUp">
                            <span class="fa fa-credit-card"></span> Billing
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ url('portal/account/settings') }}" class="animated animated-short fadeInUp">
                            <span class="fa fa-gear"></span> Account Settings </a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ url('auth/logout') }}" class="animated animated-short fadeInUp">
                            <span class="fa fa-power-off"></span> Logout </a>
                    </li>
                </ul>
            </li>
        </ul>

    </header>
    <!-- End: Header -->

    <!-- Start: Sidebar -->
    <aside id="sidebar_left" class="nano nano-primary affix">

        <!-- Start: Sidebar Left Content -->
        <div class="sidebar-left-content nano-content">

            <!-- Start: Sidebar Menu -->
            <ul class="nav sidebar-menu">
                <li class="sidebar-label pt20">Menu</li>
                <li class="active">
                    <a href="{{ url('admin/dashboard') }}">
                        <span class="glyphicon glyphicon-home"></span>
                        <span class="sidebar-title">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-label pt20">Data</li>
                <li>
                    <a href="{{ route('admin.brand.list') }}">
                        <span class="glyphicons glyphicons-book"></span>
                        <span class="sidebar-title">Brands</span>
                    </a>
                </li>
                @if(Session::get('') == '')
                <li>
                    <a href="{{ route('admin.manager.list') }}">
                        <span class="glyphicons glyphicons-book"></span>
                        <span class="sidebar-title">Admins</span>
                    </a>
                </li>
                @endif
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-cargo"></span>
                        <span class="sidebar-title">Competitions</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="{{ url('admin/v1/competitions/list') }}">
                                <span class="glyphicons glyphicons-show_thumbnails_with_lines"></span>
                                <span class="sidebar-title">View Competitions</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="glyphicons glyphicons-shopping_bag"></span>
                        <span class="sidebar-title">Webshop</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="{{ url('admin/v1/webshop/brand-ids') }}">
                                <span class="glyphicons glyphicons-calculator"></span>
                                <span class="sidebar-title">Brand ID's</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('admin/v1/ads') }}">
                        <span class="glyphicons glyphicons-adress_book"></span>
                        <span class="sidebar-title">Advertentions</span>
                    </a>
                </li>

                <li class="sidebar-label pt20">API Configuration</li>
                <li>
                    <a href="{{ url('admin/v1/api-keys/list') }}">
                        <span class="glyphicons glyphicons-keys"></span>
                        <span class="sidebar-title">API Keys</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin/v1/configuration') }}">
                        <span class="glyphicons glyphicons-settings"></span>
                        <span class="sidebar-title">Configuration</span>
                </li>


            <!-- Start: Sidebar Collapse Button -->
            <div class="sidebar-toggle-mini">
                <a href="#">
                    <span class="fa fa-sign-out"></span>
                </a>
            </div>
            <!-- End: Sidebar Collapse Button -->

        </div>
        <!-- End: Sidebar Left Content -->

    </aside>

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

        <!-- Begin: Content -->
        <section id="content" class="animated fadeIn">
            @yield('content')
        </section>
        <!-- End: Content -->

    </section>

</div>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('admin-assets/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('admin-assets/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

<!-- Theme Javascript -->
<script src="{{ asset('admin-assets/assets/js/utility/utility.js') }}"></script>
<script src="{{ asset('admin-assets/assets/js/demo/demo.js') }}"></script>
<script src="{{ asset('admin-assets/assets/js/main.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {

        "use strict";

        // Init Theme Core
        Core.init();

    });
</script>
<!-- END: PAGE SCRIPTS -->

@yield('scripts')

</body>

</html>
