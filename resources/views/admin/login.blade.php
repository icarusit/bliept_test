<!DOCTYPE html>
<html>

<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Studio 100 - Bliep - Login</title>
    <meta name="author" content="Stijn Debakker">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/skin/default_skin/css/theme.css') }}">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-assets/assets/admin-tools/admin-forms/css/admin-forms.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body class="external-page external-alt sb-l-c sb-r-c">

<!-- Start: Main -->
<div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

        <!-- Begin: Content -->
        <section id="content">

            <div class="admin-form theme-info mw500" id="login">

                <!-- Login Logo -->
                <div class="row table-layout">
                    <a href="dashboard.html" title="Return to Dashboard">
                        <img src="{{ url('logo.png') }}" title="Studio 100 Logo" class="center-block img-responsive" style="max-width: 275px;">
                    </a>
                </div>

                <!-- Login Panel/Form -->
                <div class="panel mt30 mb25">

                    <form method="post" action="{{ url('auth/login') }}" id="contact">
                        <div class="panel-body bg-light p25 pb15">
                            <!-- Username Input -->
                            <div class="section">
                                <label for="email" class="field-label text-muted fs18 mb10">Email</label>
                                <label for="email" class="field prepend-icon">
                                    <input type="email" name="email" id="email" class="gui-input" placeholder="Enter email" required>
                                    <label for="email" class="field-icon">
                                        <i class="fa fa-envelope"></i>
                                    </label>
                                </label>
                            </div>

                            <!-- Password Input -->
                            <div class="section">
                                <label for="username" class="field-label text-muted fs18 mb10">Password</label>
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password" required>
                                    <label for="password" class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </label>
                                </label>
                            </div>

                        </div>

                        <div class="panel-footer clearfix">
                            <button type="submit" class="button btn-primary mr10 pull-right">Sign In</button>
                            <label class="switch ib switch-primary mt10">
                                <input type="checkbox" name="remember_me" id="remember" checked>
                                <label for="remember" data-on="YES" data-off="NO"></label>
                                <span>Remember me</span>
                            </label>
                        </div>

                        {!! csrf_field() !!}

                    </form>
                </div>
            </div>

        </section>
        <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

</div>
<!-- End: Main -->


<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('admin-assets/vendor/jquery/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('admin-assets/vendor/jquery/jquery_ui/jquery-ui.min.js') }}"></script>

<!-- Theme Javascript -->
<script src="{{ asset('admin-assets/assets/js/utility/utility.js') }}"></script>
<script src="{{ asset('admin-assets/assets/js/demo/demo.js') }}"></script>
<script src="{{ asset('admin-assets/assets/js/main.js') }}"></script>

<!-- Page Javascript -->
<script type="text/javascript">
    jQuery(document).ready(function() {

        "use strict";

        // Init Theme Core
        Core.init();

    });
</script>

<!-- END: PAGE SCRIPTS -->

</body>

</html>
