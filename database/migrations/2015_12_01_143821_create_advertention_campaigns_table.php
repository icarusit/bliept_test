<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertentionCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertention_campaigns', function(Blueprint $table){
            $table->increments('id');

            $table->integer('brand_id')->nullable();

            $table->string('title');
            $table->text('description')->nullable();
            $table->string('customer')->nullable();
            $table->string('website_url')->nullable();

            $table->boolean('active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
