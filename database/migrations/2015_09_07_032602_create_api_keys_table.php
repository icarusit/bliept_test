<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_keys', function(Blueprint $table){
            $table->increments('id');

            $table->string('api_key');
            $table->string('issuer_id');
            $table->boolean('full_rights')->default(false);

            $table->boolean('enabled')->default(true);

            $table->string('ip_adresses')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_keys');
    }
}
