<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function(Blueprint $table){

            $table->string('id')->primary();
            $table->string('parent_brand_id')->nullable()->index();
            $table->string('language_id')->nullable()->index();
            $table->string('country_id')->nullable()->index();

            $table->string('name')->nullable();
            $table->enum('brand_type', ['Master', 'Child'])->default('Master');
            $table->string('migration_id')->nullable()->index();

            $table->text('description')->nullable();

            $table->boolean('active')->default(true);

            $table->string('thumbnail_url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('youtube_url')->nullable();
            $table->string('web_url')->nullable();
            $table->string('webshop_category_url')->nullable();

            $table->integer('nps_score')->nullable();
            $table->boolean('publicly_visible')->default(true);

            $table->integer('display_order')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brands');
    }
}
