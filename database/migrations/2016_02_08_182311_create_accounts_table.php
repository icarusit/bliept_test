<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function(Blueprint $table){
            $table->string('id')->primary();
            $table->string('country_id')->nullable()->index();
            $table->string('language_id')->nullable()->index();
            $table->string('parent_account_id')->nullable()->index();
            $table->enum('account_type', ['Child', 'Parent', 'Staff', 'Press', 'Admin'])->default('Parent')->index();

            $table->string('parent_email')->nullable();

            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('middlename')->nullable();
            $table->enum('gender', ['M', 'F'])->nullable();

            $table->date('birthdate')->nullable();
            $table->string('email')->nullable()->index();
            $table->string('password')->nullable()->index();

            $table->string('saludation')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->text('description')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_number')->nullable();
            $table->string('address_postal_code')->nullable();            
            $table->string('address_city')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();

            $table->boolean('b2b')->default(false);
            $table->boolean('checked_agility')->default(false);
            $table->string('company')->nullable();
            $table->string('company_email')->nullable();
            $table->string('vat_number')->nullable();
            $table->string('company_vat_number')->nullable();

            $table->string('migration_id')->nullable()->index();

            $table->string('facebook_id')->nullable();
            $table->string('google_plus_id')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('linkedin_id')->nullable();

            $table->string('job_title')->nullable();

            $table->dateTime('last_contact_time')->nullable();

            $table->enum('subscribe_status', ['OptIn', 'OptOut'])->default('OptOut')->index();
            $table->dateTime('optin_date')->nullable();
            $table->dateTime('optout_date')->nullable();
            $table->string('subscribe_source')->nullable();
            $table->string('unsubscribe_source')->nullable();

            $table->enum('partner_subscribe_status', ['OptIn', 'OptOut'])->default('OptOut')->index();
            $table->dateTime('partner_optin_date')->nullable();
            $table->dateTime('partner_optout_date')->nullable();
            $table->string('partner_subscribe_source')->nullable();
            $table->string('partner_unsubscribe_source')->nullable();

            $table->string('preferred_device')->nullable();
            $table->string('security_hash')->nullable()->index();

            $table->boolean('is_active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts');
    }
}
