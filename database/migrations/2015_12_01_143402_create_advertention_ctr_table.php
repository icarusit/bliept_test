<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertentionCtrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertention_ctr', function(Blueprint $table){
            $table->increments('id');

	        $table->integer('campaign_id')->unsigned()->index();
	        $table->integer('image_id')->unsigned()->index();

	        $table->string('public_ip')->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
