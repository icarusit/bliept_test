<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAdvertentionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advertention_images', function(Blueprint $table){
            $table->integer('impressions')->default(0)->after('encoded_image_url');
            $table->integer('clicks')->default(0)->after('impressions');
        });

        $images = App\Models\Utils\Advertention\Image::all();
        foreach($images as $image)
        {
            $impressionCount = DB::table('advertention_impressions')->where('image_id', $image->id)->count('id');
            $clicksCount = DB::table('advertention_ctr')->where('image_id', $image->id)->count('id');

            $image->clicks = $clicksCount;
            $image->impressions = $impressionCount;
            $image->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advertention_images', function(Blueprint $table){
            $table->dropColumn('impressions');
            $table->dropColumn('clicks');
        });
    }
}
