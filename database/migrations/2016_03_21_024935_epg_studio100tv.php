<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EpgStudio100tv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epg_studio100tv', function(Blueprint $table){
            $table->increments('id');

            $table->string('program_id')->nullable();
            $table->date('program_date')->nullable();
            $table->string('day_name')->nullable();
            $table->date('onscreen_date')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->dateTime('length')->nullable();
            $table->string('show_title')->nullable();
            $table->integer('show_year')->nullable();
            $table->string('country')->nullable();
            $table->string('episode_title')->nullable();
            $table->integer('episode_number')->nullable();
            $table->integer('episode_count')->nullable();
            $table->string('jaargang')->nullable();
            $table->text('cast')->nullable();
            $table->text('synopsis')->nullable();
            $table->string('category')->nullable();
            $table->boolean('tn_replay_rights')->nullable();
            $table->integer('tn_replay_days')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('epg_studio100tv');
    }
}
