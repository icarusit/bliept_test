<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToBrandIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('webshop_brand_ids', function(Blueprint $table){
            $table->string('name')->nullable()->after('sf_brand_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('webshop_brand_ids', function(Blueprint $table){
            $table->removeColumn('name');
        });
    }
}
