<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebshopProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webshop_products', function(Blueprint $table){
            $table->increments('id');

            $table->integer('webshop_order_id')->unsigned();
            $table->string('brand_id')->nullable();

            $table->string('product_code')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->decimal('price_excl')->nullable();
            $table->decimal('price_incl')->nullable();
            $table->decimal('vat_amount')->nullable();
            $table->decimal('discount')->nullable();
            $table->string('product_category')->nullable();
            $table->integer('quantity')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('webshop_products');
    }
}
