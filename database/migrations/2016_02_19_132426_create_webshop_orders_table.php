<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebshopOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webshop_orders', function(Blueprint $table){
            $table->increments('id');

            $table->string('user_id')->nullable()->index();
            $table->integer('extern_order_id')->nullable();

            $table->date('order_date')->nullable();
            $table->decimal('total_order_value')->nullable();
            $table->decimal('netto_order_value')->nullable();

            $table->string('visit_origin')->nullable();
            $table->integer('number_of_lines')->nullable();

            $table->decimal('shipping_costs_excl_vat')->nullable();
            $table->integer('nps')->nullable();

            $table->decimal('discount')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
