<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EpgJuniortv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epg_juniortv_programs', function(Blueprint $table){
            $table->increments('id');

            $table->string('program_reference_number')->index();
            $table->string('series_reference_number')->nullable();
            $table->string('program_type')->nullable();
            $table->string('title')->nullable();
            $table->string('original_title')->nullable();
            $table->string('episode_title')->nullable();
            $table->string('original_episode_title')->nullable();
            $table->string('episode_index')->nullable();
            $table->string('season')->nullable();
            $table->string('genre')->nullable();
            $table->string('production_countries')->nullable();
            $table->string('production_years')->nullable();
            $table->string('aspect_ratio')->nullable();
            $table->text('description_short')->nullable();
            $table->string('resource')->nullable();

            $table->timestamps();
        });

        Schema::create('epg_juniortv_broadcast', function(Blueprint $table){
            $table->increments('id');

            $table->string('event_id')->nullable();
            $table->string('program_reference_number')->index();
            $table->dateTime('displayed_time_start')->nullable();
            $table->dateTime('displayed_time_end')->nullable();
            $table->dateTime('real_time_start')->nullable();
            $table->dateTime('real_time_end')->nullable();
            $table->string('parental_rating')->nullable();
            $table->string('aspect_ratio')->nullable();
            $table->time('duration')->nullable();
            $table->boolean('first_episode')->nullable();
            $table->boolean('pilot')->nullable();
            $table->boolean('live')->nullable();
            $table->boolean('tv_premiere')->nullable();
            $table->boolean('hd_premiere')->nullable();

            $table->timestamps();
        });
    }

    /** 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('epg_juniortv_programs');
        Schema::drop('epg_juniortv_broadcast');
    }
}
