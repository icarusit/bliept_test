<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatisticsColumnsToBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function(Blueprint $table){
            $table->bigInteger('web_visits')->default(0)->after('display_order');
            $table->bigInteger('brand_subscribes')->default(0)->after('web_visits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function(Blueprint $table){
            $table->dropColumn('web_visits');
            $table->dropColumn('brand_subscribes');
        });
    }
}
