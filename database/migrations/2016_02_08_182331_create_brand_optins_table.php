<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandOptinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_optins', function(Blueprint $table){
            $table->string('id')->primary();

            $table->string('brand_id')->index();
            $table->enum('subscribe_status', ['OptIn', 'OptOut', 'Neutral'])->index();

            $table->string('subscribe_source')->nullable();
            $table->dateTime('subscribe_date')->nullable();

            $table->string('unsubscribe_source')->nullable();
            $table->dateTime('unsubscribe_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brand_optins');
    }
}
