<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertentionImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertention_images', function(Blueprint $table){
            $table->increments('id');

            $table->integer('campaign_id')->unsigned();
            $table->string('thumbnail_url')->nullable();
            $table->string('image_url');
	        $table->string('website_url')->nullable();

            $table->string('encoded_url')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
