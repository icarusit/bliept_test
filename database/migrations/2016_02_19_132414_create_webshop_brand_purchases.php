<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebshopBrandPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webshop_brand_purchases', function(Blueprint $table){
            $table->increments('id');

            $table->integer('webshop_order_id')->unsigned()->index();
            $table->string('brand_id')->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('webshop_brand_purchases');
    }
}
