<?php
return array(

    /*
	|--------------------------------------------------------------------------
	| Default FTP Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the FTP connections below you wish
	| to use as your default connection for all ftp work.
	|
	*/

    'default' => 'connection1',

    /*
    |--------------------------------------------------------------------------
    | FTP Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the FTP connections setup for your application.
    |
    */

    'connections' => array(

        'studio100tv' => array(
            'host'   => 'ftp.videohouse.be',
            'port'  => 21,
            'username' => 'Magnet Media Studio100',
            'password'   => 'jGjDCD0A',
            'passive'   => true,
        ),

        'njamtv' => [
            'host'   => 'ftp.videohouse.be',
            'port'  => 21,
            'username' => 'Magnet Media NJAM!',
            'password'   => '5IjMzTvYk7',
            'passive'   => true,
        ],

        'juniortv' => array(
            'host' => 'ftp.studio100.com',
            'port' => 21,
            'username' => 'junior-epg',
            'password' => 'hJbt3-67!',
            'passive' => true
        )
    ),
);